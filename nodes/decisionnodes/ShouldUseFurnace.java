package scripts.modules.furnacecrafting.nodes.decisionnodes;

import scripts.fluffeesapi.client.clientextensions.Interfaces;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.furnacecrafting.data.Variables;
import scripts.modules.furnacecrafting.nodes.processnodes.CraftItems;
import scripts.modules.furnacecrafting.nodes.processnodes.UseFurnace;

public class ShouldUseFurnace extends ConstructorDecisionNode {

    private Variables variables;

    @Override
    public boolean isValid() {
        return Interfaces.isInterfaceSubstantiated(variables.getCraftingInterface());
    }

    @Override
    public void initializeNode() {
        setTrueNode(new CraftItems(variables));
        setFalseNode(new UseFurnace(variables));
    }

    public static ShouldUseFurnace create(Variables variables) {
        ShouldUseFurnace node = new ShouldUseFurnace();
        node.variables = variables;
        node.initializeNode();
        return node;
    }

}
