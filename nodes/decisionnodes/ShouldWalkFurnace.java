package scripts.modules.furnacecrafting.nodes.decisionnodes;

import org.tribot.api2007.Player;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.furnacecrafting.data.Variables;
import scripts.modules.furnacecrafting.nodes.processnodes.WalkFurnace;

public class ShouldWalkFurnace extends BaseDecisionNode {

    private Variables variables;

    @Override
    public boolean isValid() {
        return !variables.getFurnaceArea().contains(Player.getPosition());
    }

    @Override
    public void initializeNode() {
        setTrueNode(new WalkFurnace(variables));
        setFalseNode(ShouldUseFurnace.create(variables));
    }

    public static ShouldWalkFurnace create(Variables variables) {
        ShouldWalkFurnace node = new ShouldWalkFurnace();
        node.variables = variables;
        node.initializeNode();
        return node;
    }

}
