package scripts.modules.furnacecrafting.nodes.processnodes;

import org.tribot.api.Clicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.types.RSObject;
import scripts.fluffeesapi.client.clientextensions.Objects;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.SuccessProcessNode;
import scripts.fluffeesapi.utilities.Conditions;
import scripts.modules.furnacecrafting.data.Variables;

public class UseFurnace extends SuccessProcessNode {

    private Variables variables;

    public UseFurnace(Variables variables) {
        this.variables = variables;
    }

    @Override
    public String getStatus() {
        return "Using furnace";
    }

    @Override
    public void successExecute() {
        RSObject[] furnaces = Objects.findNearest(7, variables.getFurnace());
        if (furnaces.length == 0) {
            return;
        }
        if (Clicking.click( "Smelt", furnaces[0])) {
            Timing.waitCondition(Conditions.interfaceSubstantiated(
                    variables.getCraftingInterface()
                ),
                General.random(3000, 5000)
            );
        }
    }
}