package scripts.modules.furnacecrafting.nodes.processnodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.WebWalking;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.SuccessProcessNode;
import scripts.fluffeesapi.utilities.Conditions;
import scripts.modules.furnacecrafting.data.Variables;

public class WalkFurnace extends SuccessProcessNode {

    private Variables variables;

    public WalkFurnace(Variables variables) {
        this.variables = variables;
    }

    @Override
    public String getStatus() {
        return "Walking to furnace";
    }

    @Override
    public void successExecute() {
        WebWalking.walkTo(variables.getFurnaceArea().getRandomTile());
        Timing.waitCondition(Conditions.areaContains(variables.getFurnaceArea()), General.random(3000, 5000));
    }
}