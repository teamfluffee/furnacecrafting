package scripts.modules.furnacecrafting.nodes.processnodes;

import org.tribot.api.Clicking;
import org.tribot.api.General;
import org.tribot.api2007.Login;
import org.tribot.api2007.Skills;
import org.tribot.api2007.types.RSInterface;
import scripts.fluffeesapi.client.clientextensions.Interfaces;
import scripts.fluffeesapi.client.clientextensions.Inventory;
import scripts.fluffeesapi.data.structures.AntibanVariables;
import scripts.fluffeesapi.scripting.antiban.AntiBanSingleton;
import scripts.fluffeesapi.scripting.antiban.AntibanUtilities;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.SuccessProcessNode;
import scripts.fluffeesapi.scripting.frameworks.task.Priority;
import scripts.fluffeesapi.scripting.frameworks.task.tasktypes.AntibanTask;
import scripts.modules.furnacecrafting.data.Variables;

public class CraftItems extends AntibanTask {

    private int startingLevel, targetXp;

    public CraftItems(Variables variables) {
        super(variables);
    }

    @Override
    public Priority priority() {
        return Priority.MEDIUM;
    }

    @Override
    public boolean isValid() {
        return Interfaces.isInterfaceValid(((Variables) variables).getCraftingInterface());
    }

    @Override
    public String getStatus() {
        return "Crafting items";
    }

    @Override
    public void preActivitySetup() {
        startingLevel = Skills.getCurrentLevel(Skills.SKILLS.CRAFTING);
        targetXp = (int) (Skills.getXP(Skills.SKILLS.CRAFTING) + ((Variables) variables).getCraftingItem().getXpGained()
                * Inventory.getCount(((Variables) variables).getCraftingItem().getMaterials()[0]));
    }

    @Override
    public boolean getStoppingCondition() {
        return Skills.getCurrentLevel(Skills.SKILLS.CRAFTING) > startingLevel ||
                Skills.getXP(Skills.SKILLS.CRAFTING) >= targetXp;
    }


    @Override
    public boolean beginAntibanActivity() {
        RSInterface craftingInterface = Interfaces.get(((Variables) variables).getCraftingInterface());
        return craftingInterface != null && Clicking.click("Make", craftingInterface);
    }
}