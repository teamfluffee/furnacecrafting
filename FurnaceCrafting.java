package scripts.modules.furnacecrafting;

import java.util.concurrent.atomic.AtomicInteger;

import org.tribot.api2007.Skills;

import scripts.fluffeesapi.client.clientextensions.Inventory;
import scripts.fluffeesapi.data.skilling.crafting.CraftingItem;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.TreeMission;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;
import scripts.modules.furnacecrafting.data.Furnaces;
import scripts.modules.furnacecrafting.data.Variables;
import scripts.modules.furnacecrafting.nodes.decisionnodes.ShouldWalkFurnace;

public class FurnaceCrafting implements TreeMission {

    private Variables variables;

    public FurnaceCrafting(AtomicInteger numberSleeps, AtomicInteger averageAntibanSleep,
                           CraftingItem craftingItem, Furnaces furnace) {
        try {
            this.variables = new Variables(numberSleeps, averageAntibanSleep, craftingItem, furnace);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getMissionName() {
        return "Furnace Crafting";
    }

    @Override
    public boolean isMissionValid() {
        return variables.getCraftingItem().getLevelRequired() <= Skills.getActualLevel(Skills.SKILLS.CRAFTING) &&
                Inventory.inventoryContainsAll(variables.getCraftingMethod().getCraftingEquipment()) &&
                Inventory.inventoryContainsAll(variables.getCraftingItem().getMaterials());
    }

    @Override
    public boolean isMissionCompleted() {
        return !isMissionValid();
    }

    @Override
    public BaseDecisionNode getTreeRoot() {
        return ShouldWalkFurnace.create(variables);
    }
}
