package scripts.modules.furnacecrafting;

import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Painting;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.Mission;
import scripts.fluffeesapi.scripting.frameworks.mission.scriptTypes.MissionScript;
import scripts.fluffeesapi.scripting.painting.scriptPaint.ScriptPaint;
import scripts.fluffeesapi.scripting.swingcomponents.gui.AbstractWizardGui;
import scripts.modules.furnacecrafting.data.Furnaces;
import scripts.modules.furnacecrafting.data.items.Rings;

@ScriptManifest(
        authors     = "Fluffee",
        category    = "Sub Scripts",
        name        = "Furnace Crafting",
        description = "The hottest kind of crafting.",
        gameMode = 1)

public class FurnaceCraftingScript extends MissionScript implements Painting {

    private ScriptPaint scriptPaint = new ScriptPaint.Builder(
                ScriptPaint.hex2Rgb("#ff0054"), "Furnace Crafting")
            .addField("Version", Double.toString(1.00))
            .build();

    @Override
    public Mission getMission() {
        return new FurnaceCrafting(Rings.SAPPHIRE_RING, Furnaces.EDGEVILLE_FURNACE);
    }

    @Override
    public AbstractWizardGui getGUI() {
        return null;
    }

    @Override
    public ScriptPaint getScriptPaint() {
        return scriptPaint;
    }
}
