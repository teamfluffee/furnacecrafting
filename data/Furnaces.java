package scripts.modules.furnacecrafting.data;

import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;

@SuppressWarnings("unused")
public enum Furnaces {

    EDGEVILLE_FURNACE( new RSArea(new RSTile(3105, 3502, 0), new RSTile(3111, 3496, 0)));

    private RSArea furnaceArea;

    Furnaces(RSArea furnaceArea) {
        this.furnaceArea = furnaceArea;
    }

    public RSArea getFurnaceArea() {
        return furnaceArea;
    }

}
