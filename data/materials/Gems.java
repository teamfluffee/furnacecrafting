package scripts.modules.furnacecrafting.data.materials;

import scripts.fluffeesapi.data.interactables.Interactable;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMaterial;

import static scripts.fluffeesapi.data.interactables.Interactable.DEFAULT_ID;

@SuppressWarnings("unused")
public enum Gems implements CraftingMaterial {

    SAPPHIRE(new Interactable("Sapphire", "Use", DEFAULT_ID)),
    EMERALD(new Interactable("Emerald", "Use", DEFAULT_ID)),
    RUBY(new Interactable("Ruby", "Use", DEFAULT_ID)),
    DIAMOND(new Interactable("Diamond", "Use", DEFAULT_ID)),
    DRAGONSTONE(new Interactable("Dragonstone", "Use", DEFAULT_ID)),
    ENCHANTED_GEM(new Interactable("Enchanted gem", "Use", DEFAULT_ID)),
    ETERNAL_GEM(new Interactable("Eternal gem", "Use", DEFAULT_ID)),
    ONYX(new Interactable("Onyx", "Use", DEFAULT_ID)),
    ZENYTE(new Interactable("Zenyte", "Use", DEFAULT_ID)),

    OPAL(new Interactable("Opal", "Use", DEFAULT_ID)),
    JADE(new Interactable("Jade", "Use", DEFAULT_ID)),
    RED_TOPAZ(new Interactable("Red topaz", "Use", DEFAULT_ID));

    private Interactable material;

    Gems(Interactable material) {
        this.material = material;
    }

    @Override
    public Interactable getMaterial() {
        return material;
    }
}
