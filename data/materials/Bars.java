package scripts.modules.furnacecrafting.data.materials;

import scripts.fluffeesapi.data.interactables.Interactable;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMaterial;

import static scripts.fluffeesapi.data.interactables.Interactable.DEFAULT_ID;

@SuppressWarnings("unused")
public enum Bars implements CraftingMaterial {

    GOLD_BAR(new Interactable("Gold bar", "Use", DEFAULT_ID)),
    SILVER_BAR(new Interactable("Silver bar", "Use", DEFAULT_ID));

    private Interactable material;

    Bars(Interactable material) {
        this.material = material;
    }

    @Override
    public Interactable getMaterial() {
        return material;
    }
}
