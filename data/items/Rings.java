package scripts.modules.furnacecrafting.data.items;

import scripts.fluffeesapi.data.interactables.Interactable;
import scripts.fluffeesapi.data.interactables.InteractableItem;
import scripts.fluffeesapi.data.skilling.crafting.CraftingItem;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMaterial;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMethods;
import scripts.modules.furnacecrafting.data.Methods;
import scripts.modules.furnacecrafting.data.materials.Bars;
import scripts.modules.furnacecrafting.data.materials.Gems;

import static scripts.fluffeesapi.data.interactables.Interactable.DEFAULT_ID;

@SuppressWarnings("unused")
public enum Rings implements CraftingItem {

    GOLD_RING(
        5,
        15,
        new Interactable("Gold ring", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1)
    ),
    SAPPHIRE_RING(
        20,
        40,
        new Interactable("Sapphire ring", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.SAPPHIRE, 1)
    ),
    EMERALD_RING(
        27,
        55,
        new Interactable("Emerald ring", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.EMERALD, 1)
    ),
    RUBY_RING(
        34,
        70,
        new Interactable("Ruby ring", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.RUBY, 1)
    ),
    DIAMOND_RING(
        43,
        85,
        new Interactable("Diamond ring", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.DIAMOND, 1)
    ),
    DRAGONSTONE_RING(
        55,
        100,
        new Interactable("Dragonstone ring", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.DRAGONSTONE, 1)
    ),
    SLAYER_RING(
        75,
        15,
        new Interactable("Slayer ring", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.ENCHANTED_GEM, 1)
    ),
    ETERNAL_RING(
        75,
        15,
        new Interactable("Slayer ring (eternal)", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.ETERNAL_GEM, 1)
    ),
    ONYX_RING(
        67,
        115,
        new Interactable("Onyx ring", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.ONYX, 1)
    ),
    ZENYTE_RING(
        89,
        150,
        new Interactable("Zenyte ring", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.ZENYTE, 1)
    ),
    OPAL_RING(
        1,
        10,
        new Interactable("Opal ring", "Use", DEFAULT_ID),
        makeMaterial(Bars.SILVER_BAR, 1),
        makeMaterial(Gems.OPAL, 1)
    ),
    JADE_RING(
        13,
        32,
        new Interactable("Jade ring", "Use", DEFAULT_ID),
        makeMaterial(Bars.SILVER_BAR, 1),
        makeMaterial(Gems.JADE, 1)
    ),
    TOPAZ_RING(
        16,
        35,
        new Interactable("Topaz ring", "Use", DEFAULT_ID),
        makeMaterial(Bars.SILVER_BAR, 1),
        makeMaterial(Gems.RED_TOPAZ, 1)
    );


    private int levelRequired;
    private double xpGained;
    private Interactable product;
    private InteractableItem[] materials;

    Rings(int levelRequired, double xpGained, Interactable product, InteractableItem... materials) {
        this.levelRequired = levelRequired;
        this.xpGained = xpGained;
        this.product = product;
        this.materials = materials;
    }

    @Override
    public int getLevelRequired() {
        return levelRequired;
    }

    @Override
    public double getXpGained() {
        return xpGained;
    }

    @Override
    public Interactable getProduct() {
        return product;
    }

    @Override
    public InteractableItem[] getMaterials() {
        return materials;
    }

    @Override
    public CraftingMethods getMethod() {
        return Methods.RING;
    }

    public static InteractableItem makeMaterial(CraftingMaterial material, int quantity) {
        return CraftingItem.makeMaterial(material, quantity);
    }
}
