package scripts.modules.furnacecrafting.data.items;

import scripts.fluffeesapi.data.interactables.Interactable;
import scripts.fluffeesapi.data.interactables.InteractableItem;
import scripts.fluffeesapi.data.skilling.crafting.CraftingItem;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMaterial;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMethods;
import scripts.modules.furnacecrafting.data.Methods;
import scripts.modules.furnacecrafting.data.materials.Bars;
import scripts.modules.furnacecrafting.data.materials.Gems;

import static scripts.fluffeesapi.data.interactables.Interactable.DEFAULT_ID;

@SuppressWarnings("unused")
public enum Necklaces implements CraftingItem {

    GOLD_NECKLACE(
        6,
        20,
        new Interactable("Gold necklace", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1)
    ),
    SAPPHIRE_NECKLACE(
        22,
        55,
        new Interactable("Sapphire necklace", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.SAPPHIRE, 1)
    ),
    EMERALD_NECKLACE(
        29,
        60,
        new Interactable("Emerald necklace", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.EMERALD, 1)
    ),
    RUBY_NECKLACE(
        40,
        75,
        new Interactable("Ruby necklace", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.RUBY, 1)
    ),
    DIAMOND_NECKLACE(
        56,
        90,
        new Interactable("Diamond necklace", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.DIAMOND, 1)
    ),
    DRAGONSTONE_NECKLACE(
        72,
        105,
        new Interactable("Dragonstone necklace", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.DRAGONSTONE, 1)
    ),
    ONYX_NECKLACE(
        82,
        120,
        new Interactable("Onyx necklace", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.ONYX, 1)
    ),
    ZENYTE_NECKLACE(
        92,
        165,
        new Interactable("Zenyte necklace", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.ZENYTE, 1)
    ),
    OPAL_NECKLACE(
        16,
        35,
        new Interactable("Opal necklace", "Use", DEFAULT_ID),
        makeMaterial(Bars.SILVER_BAR, 1),
        makeMaterial(Gems.OPAL, 1)
    ),
    JADE_NECKLACE(
        25,
        54,
        new Interactable("Jade necklace", "Use", DEFAULT_ID),
        makeMaterial(Bars.SILVER_BAR, 1),
        makeMaterial(Gems.JADE, 1)
    ),
    TOPAZ_NECKLACE(
        32,
        70,
        new Interactable("Topaz necklace", "Use", DEFAULT_ID),
        makeMaterial(Bars.SILVER_BAR, 1),
        makeMaterial(Gems.RED_TOPAZ, 1)
    );


    private int levelRequired;
    private double xpGained;
    private Interactable product;
    private InteractableItem[] materials;

    Necklaces(int levelRequired, double xpGained, Interactable product, InteractableItem... materials) {
        this.levelRequired = levelRequired;
        this.xpGained = xpGained;
        this.product = product;
        this.materials = materials;
    }

    @Override
    public int getLevelRequired() {
        return levelRequired;
    }

    @Override
    public double getXpGained() {
        return xpGained;
    }

    @Override
    public Interactable getProduct() {
        return product;
    }

    @Override
    public InteractableItem[] getMaterials() {
        return materials;
    }

    @Override
    public CraftingMethods getMethod() {
        return Methods.NECKLACE;
    }

    public static InteractableItem makeMaterial(CraftingMaterial material, int quantity) {
        return CraftingItem.makeMaterial(material, quantity);
    }
}