package scripts.modules.furnacecrafting.data.items;

import scripts.fluffeesapi.data.interactables.Interactable;
import scripts.fluffeesapi.data.interactables.InteractableItem;
import scripts.fluffeesapi.data.skilling.crafting.CraftingItem;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMaterial;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMethods;
import scripts.modules.furnacecrafting.data.Methods;
import scripts.modules.furnacecrafting.data.materials.Bars;
import scripts.modules.furnacecrafting.data.materials.Gems;

import static scripts.fluffeesapi.data.interactables.Interactable.DEFAULT_ID;

@SuppressWarnings("unused")
public enum Bracelets implements CraftingItem {

    GOLD_BRACELET(
        7,
        25,
        new Interactable("Gold bracelet", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1)
    ),
    SAPPHIRE_BRACELET(
        23,
        60,
        new Interactable("Sapphire bracelet", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.SAPPHIRE, 1)
    ),
    EMERALD_BRACELET(
        30,
        65,
        new Interactable("Emerald bracelet", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.EMERALD, 1)
    ),
    RUBY_BRACELET(
        42,
        80,
        new Interactable("Ruby bracelet", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.RUBY, 1)
    ),
    DIAMOND_BRACELET(
        58,
        95,
        new Interactable("Diamond bracelet", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.DIAMOND, 1)
    ),
    DRAGONSTONE_BRACELET(
        74,
        110,
        new Interactable("Dragonstone bracelet", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.DRAGONSTONE, 1)
    ),
    ONYX_BRACELET(
        84,
        125,
        new Interactable("Onyx bracelet", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.ONYX, 1)
    ),
    ZENYTE_BRACELET(
        95,
        180,
        new Interactable("Zenyte bracelet", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.ZENYTE, 1)
    ),
    OPAL_BRACELET(
        22,
        45,
        new Interactable("Opal bracelet", "Use", DEFAULT_ID),
        makeMaterial(Bars.SILVER_BAR, 1),
        makeMaterial(Gems.OPAL, 1)
    ),
    JADE_BRACELET(
        29,
        60,
        new Interactable("Jade bracelet", "Use", DEFAULT_ID),
        makeMaterial(Bars.SILVER_BAR, 1),
        makeMaterial(Gems.JADE, 1)
    ),
    TOPAZ_BRACELET(
        38,
        75,
        new Interactable("Topaz bracelet", "Use", DEFAULT_ID),
        makeMaterial(Bars.SILVER_BAR, 1),
        makeMaterial(Gems.RED_TOPAZ, 1)
    );


    private int levelRequired;
    private double xpGained;
    private Interactable product;
    private InteractableItem[] materials;

    Bracelets(int levelRequired, double xpGained, Interactable product, InteractableItem... materials) {
        this.levelRequired = levelRequired;
        this.xpGained = xpGained;
        this.product = product;
        this.materials = materials;
    }

    @Override
    public int getLevelRequired() {
        return levelRequired;
    }

    @Override
    public double getXpGained() {
        return xpGained;
    }

    @Override
    public Interactable getProduct() {
        return product;
    }

    @Override
    public InteractableItem[] getMaterials() {
        return materials;
    }

    @Override
    public CraftingMethods getMethod() {
        return Methods.AMULET;
    }

    public static InteractableItem makeMaterial(CraftingMaterial material, int quantity) {
        return CraftingItem.makeMaterial(material, quantity);
    }
}
