package scripts.modules.furnacecrafting.data.items;

import scripts.fluffeesapi.data.interactables.Interactable;
import scripts.fluffeesapi.data.interactables.InteractableItem;
import scripts.fluffeesapi.data.skilling.crafting.CraftingItem;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMaterial;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMethods;
import scripts.modules.furnacecrafting.data.Methods;
import scripts.modules.furnacecrafting.data.materials.Bars;
import scripts.modules.furnacecrafting.data.materials.Gems;

import static scripts.fluffeesapi.data.interactables.Interactable.DEFAULT_ID;

@SuppressWarnings("unused")
public enum Amulets implements CraftingItem {

    GOLD_AMULET(
        8,
        30,
        new Interactable("Gold amulet", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1)
    ),
    SAPPHIRE_AMULET(
        24,
        65,
        new Interactable("Sapphire amulet", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.SAPPHIRE, 1)
    ),
    EMERALD_AMULET(
        31,
        70,
        new Interactable("Emerald amulet", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.EMERALD, 1)
    ),
    RUBY_AMULET(
        50,
        85,
        new Interactable("Ruby amulet", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.RUBY, 1)
    ),
    DIAMOND_AMULET(
        70,
        100,
        new Interactable("Diamond amulet", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.DIAMOND, 1)
    ),
    DRAGONSTONE_AMULET(
        80,
        150,
        new Interactable("Dragonstone amulet", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.DRAGONSTONE, 1)
    ),
    ONYX_AMULET(
        90,
        165,
        new Interactable("Onyx amulet", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.ONYX, 1)
    ),
    ZENYTE_AMULET(
        98,
        200,
        new Interactable("Zenyte amulet", "Use", DEFAULT_ID),
        makeMaterial(Bars.GOLD_BAR, 1),
        makeMaterial(Gems.ZENYTE, 1)
    ),
    OPAL_AMULET(
        27,
        55,
        new Interactable("Opal amulet", "Use", DEFAULT_ID),
        makeMaterial(Bars.SILVER_BAR, 1),
        makeMaterial(Gems.OPAL, 1)
    ),
    JADE_AMULET(
        34,
        70,
        new Interactable("Jade amulet", "Use", DEFAULT_ID),
        makeMaterial(Bars.SILVER_BAR, 1),
        makeMaterial(Gems.JADE, 1)
    ),
    TOPAZ_AMULET(
        45,
        80,
        new Interactable("Topaz amulet", "Use", DEFAULT_ID),
        makeMaterial(Bars.SILVER_BAR, 1),
        makeMaterial(Gems.RED_TOPAZ, 1)
    );


    private int levelRequired;
    private double xpGained;
    private Interactable product;
    private InteractableItem[] materials;

    Amulets(int levelRequired, double xpGained, Interactable product, InteractableItem... materials) {
        this.levelRequired = levelRequired;
        this.xpGained = xpGained;
        this.product = product;
        this.materials = materials;
    }

    @Override
    public int getLevelRequired() {
        return levelRequired;
    }

    @Override
    public double getXpGained() {
        return xpGained;
    }

    @Override
    public Interactable getProduct() {
        return product;
    }

    @Override
    public InteractableItem[] getMaterials() {
        return materials;
    }

    @Override
    public CraftingMethods getMethod() {
        return Methods.AMULET;
    }

    public static InteractableItem makeMaterial(CraftingMaterial material, int quantity) {
        return CraftingItem.makeMaterial(material, quantity);
    }
}

