package scripts.modules.furnacecrafting.data.items;

import scripts.fluffeesapi.data.interactables.Interactable;
import scripts.fluffeesapi.data.interactables.InteractableItem;
import scripts.fluffeesapi.data.skilling.crafting.CraftingItem;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMaterial;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMethods;
import scripts.modules.furnacecrafting.data.Methods;
import scripts.modules.furnacecrafting.data.materials.Bars;

import static scripts.fluffeesapi.data.interactables.Interactable.DEFAULT_ID;

@SuppressWarnings("unused")
public enum Misc implements CraftingItem {
    UNSTRUNG_SYMBOL(
        16,
        50,
        new Interactable("Unstrung symbol", "Use", DEFAULT_ID),
        Methods.HOLY,
        makeMaterial(Bars.SILVER_BAR, 1)
    ),
    UNSTRUNG_EMBLEM(
        17,
        50,
        new Interactable("Unstrung emblem", "Use", DEFAULT_ID),
        Methods.SICKLE,
        makeMaterial(Bars.SILVER_BAR, 1)
    ),
    TIARA(
        23,
        52.5,
        new Interactable("Tiara", "Use", DEFAULT_ID),
        Methods.TIARA,
        makeMaterial(Bars.SILVER_BAR, 1)
    ),
    SILVER_BOLTS(
        21,
        50,
        new Interactable("Silver bolts (unf)", "Use", DEFAULT_ID),
        Methods.BOLT,
        makeMaterial(Bars.SILVER_BAR, 1)
    );

    private int levelRequired;
    private double xpGained;
    private Interactable product;
    private InteractableItem[] materials;
    private CraftingMethods method;

    Misc(int levelRequired, double xpGained, Interactable product, CraftingMethods method, InteractableItem... materials) {
        this.levelRequired = levelRequired;
        this.xpGained = xpGained;
        this.product = product;
        this.method = method;
        this.materials = materials;
    }

    @Override
    public int getLevelRequired() {
        return levelRequired;
    }

    @Override
    public double getXpGained() {
        return xpGained;
    }

    @Override
    public Interactable getProduct() {
        return product;
    }

    @Override
    public InteractableItem[] getMaterials() {
        return materials;
    }

    @Override
    public CraftingMethods getMethod() {
        return method;
    }

    public static InteractableItem makeMaterial(CraftingMaterial material, int quantity) {
        return CraftingItem.makeMaterial(material, quantity);
    }
}
