package scripts.modules.furnacecrafting.data;

import scripts.fluffeesapi.data.interactables.InteractableItem;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMethods;

import static scripts.fluffeesapi.data.interactables.Interactable.DEFAULT_ID;

@SuppressWarnings("unused")
public enum Methods implements CraftingMethods {

    RING(
            new InteractableItem("Ring mould", "Use", DEFAULT_ID, 1)
    ),
    NECKLACE(
            new InteractableItem("Necklace mould", "Use", DEFAULT_ID, 1)
    ),
    AMULET(
            new InteractableItem("Amulet mould", "Use", DEFAULT_ID, 1)
    ),
    HOLY(
            new InteractableItem("Holy mould", "Use", DEFAULT_ID, 1)
    ),
    SICKLE(
            new InteractableItem("Sickle mould", "Use", DEFAULT_ID, 1)
    ),
    TIARA(
            new InteractableItem("Tiara mould", "Use", DEFAULT_ID, 1)
    ),
    BOLT(
            new InteractableItem("Bolt mould", "Use", DEFAULT_ID, 1)
    ),
    BRACELET(
            new InteractableItem("Bracelet mould", "Use", DEFAULT_ID, 1)
    );

    private InteractableItem[] equipment;

    Methods(InteractableItem... equipment) {
        this.equipment = equipment;
    }

    @Override
    public InteractableItem[] getCraftingEquipment() {
        return equipment;
    }
}
