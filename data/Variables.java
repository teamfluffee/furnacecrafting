package scripts.modules.furnacecrafting.data;

import org.tribot.api2007.types.RSArea;
import scripts.fluffeesapi.data.interactables.Interactable;
import scripts.fluffeesapi.data.interactables.InteractableInterface;
import scripts.fluffeesapi.data.skilling.crafting.CraftingItem;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMethods;
import scripts.fluffeesapi.data.structures.AntibanVariables;
import scripts.fluffeesapi.scripting.entityselector.finders.prefabs.InterfaceEntity;

import java.util.concurrent.atomic.AtomicInteger;

import static scripts.fluffeesapi.data.interactables.BaseInteractable.DEFAULT_ID;

public class Variables extends AntibanVariables {

    private static final int CRAFTING_MASTER_INTERFACE = 446;
    private Interactable furnace;
    private CraftingItem craftingItem;
    private CraftingMethods craftingMethod;
    private RSArea furnaceArea;
    private InteractableInterface craftingInterface;

    public Variables(AtomicInteger numberSleeps, AtomicInteger averageSleepTime,
                     CraftingItem craftingItem, Furnaces furnace) throws IllegalAccessException {
        super(numberSleeps, averageSleepTime);
        this.furnace =  new Interactable("Furnace", "Use", DEFAULT_ID);
        this.furnaceArea = furnace.getFurnaceArea();
        this.craftingItem = craftingItem;
        this.craftingMethod = craftingItem.getMethod();
        this.craftingInterface = new InteractableInterface(
                new InterfaceEntity()
                    .inMaster(CRAFTING_MASTER_INTERFACE)
                    .actionContains(craftingItem.getProduct().getName()
                ), 2, DEFAULT_ID);
    }

    public Interactable getFurnace() {
        return furnace;
    }

    public CraftingItem getCraftingItem() {
        return craftingItem;
    }

    public CraftingMethods getCraftingMethod() {
        return craftingMethod;
    }

    public RSArea getFurnaceArea() {
        return furnaceArea;
    }

    public InteractableInterface getCraftingInterface() {
        return craftingInterface;
    }
}
